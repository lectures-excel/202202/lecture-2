---
pagetitle: "Especificaciones y límites de Excel"
title: "Semana 2: Especificaciones y límites de Excel"
subtitle: Taller de Excel | [ECON-1300](https://bloqueneon.uniandes.edu.co/d2l/home/133085)
author: 
      name: Eduard-Martínez
      affiliation: Universidad de los Andes  #[`r fontawesome::fa('globe')`]()
# date: Lecture 10  #"`r format(Sys.time(), '%d %B %Y')`"
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--==================-->
<!--==================-->
## **1. Especificaciones y límites de Excel**

#### **Libros y Hojas:**

* Los libros se guardan en formatos como `.xls`, `.xlsx`, `.xlm` y `xlsb` entre otros.
* El número de hojas está limitado a la memoria disponible (el valor predeterminado es 1 hoja).

#### **Filas, Columnas y Celdas:** 

Una hoja de cálculo puede almacenar:

* 1.048.576 filas.
* 16.384 columnas.
* 255 caracteres de ancho de columna.
* 32.767 caracteres por celda.
* 2.147.483.648 celdas.

![](pics/imagen.jpg){width=70%}    

<!--==================-->
<!--==================-->
## **2. Manipular filas y columnas**

### **2.1 Edición de alto y ancho**
Puede editar el ancho (hasta 255 caracteres) o alto (hasta 409 puntos) de una columna/fila. 

![](pics/widht_column.png){width=40%}   

#### **Autoajuste:**
Haciendo doble click en el separador de las columnas se autoajusta el ancho de columna:
![](pics/imagen_4.jpg){width=60%}  

#### **Manual:**
Seleccione la columna/fila y ubique la opción **Formato** en la pestaña **Inicio** de la cinta de opciones: 
![](pics/imagen_5.jpg){width=60%}  

### **2.2 Seleccionar**

 * **Clic:** Seleccionar fila o columna.
 * **Ctrl (CMD) + clic:** Seleccionar fila o columna.
 * **Shift + clic:** Seleccionar rango fila o columna.
 * **Shift + barra de espacio:** Seleccionar fila.
 * **Ctrl (CMD) + barra de espacio:** Seleccionar columna.
 
### **2.3 Mover**

 * **Arrastrar + Shift:** Intercambia filas o columnas. 
 * **Arrastrar + Ctrl:** Duplica o reemplaza fila o columna.
 * **Arrastrar + Shift + Ctrl:** Copia y añade fila o columna.

### **2.4 Insertar y eliminar**

#### **Insertar:**

 * Clic derecho -> insertar 
 * Ctrl + "+"

#### **Eliminar:**

 * clic derecho -> eliminar
 * Ctrl + "-"

### **2.5 Ocultar y mostrar**

#### **Ocultar:**

 * **Ctrl + 0:** Oculta filas.
 * **Ctrl + 9:** Oculta columnas.

#### **Mostrar:**

 * **Ctrl + Shift + 8:** Mostrar filas.
 * **Ctrl + Shift + 9:** Mostrar columnas.
 
### **2.6 Inmovilizar paneles**

Puede inmovilizar filas o columnas de una hoja de cálculo. Seleccione una celda y ubique la opción **Inmovilizar** en la pestaña **Vista** de la cinta de opciones: 

![](pics/imagen_3.jpg){width=80%}  

<!--==================-->
<!--==================-->
## **3. Celdas y Rangos**

### **3.1 Celdas**

Una celda es la intersección entre una fila y una columna. EL nombre de una celda es la combinación de la letra de la columna y el número de la fila. 

![](pics/celdas.jpeg){width=40%}   

Puede editar, ajustar o personalizar los atributos (formato, color, borde...) de una celda. Por ejemplo, una celda puede almacenar el número 1 en formato numérico o en formato fecha. Por tanto, si lo almacena como formato de fecha corta, Excel lo interpretará como **1/01/1900**.

![](pics/imagen_7.jpg){width=60%}  

### **3.2 Rango:** 

Es un grupo de celdas fronterizas delimitadas por dos celdas las cuales se pueden describir por la letra donde se encuentran, seguido de su número. Por ejemplo, A1:C2 

![](pics/imagen_8.jpg){width=40%}  

#### **Tipos de rango:**
 
 * **Unidimensional:** Referencia de una celda.
 * **Bidimensional:** Referencian las celdas contenidas entre los nombres de dos celdas.
 * **Tridimensional:** Referencia una celda o un grupo de celdas que comienzan en una hoja y terminan en otra.
 
#### **Selección/edición de rangos:**
 
 * Puede realizar un clic izquierdo y arrastrar el mouse hasta la celda que desea abarcar.
 * Presionar tecla **Shift** y moverse con las flechas hacia la dirección deseada.
 * Escribir el rango en el cuadro de nombres.
 
#### **Renombrar un rango:**

Puede renombrar un rango de celdas. Seleccionando el rango y haciendo clic derecho sobre algunas de las celdas del rango y después seleccione la opción **Asignar nombre**: 

![](pics/rename_rango.png){width=30%}  

<!--==================-->
<!--==================-->
## **4. Task**

* Genere un libro nuevo de Excel.
* Adiciónele 3 hojas nuevas, renómbrelas (como usted prefiera) y cambie el color de las pestañas de la hoja.
* Oculte una de las 3 hojas.
* Cambie el color de las celdas D10:H17.
* Aplique un formato numérico a las celdas del rango F8:G20
* Inmovilicé las filas y columnas por encima de la fila 7 y a la izquierda de la columna C.
* Oculte las filas 20 y 21, y la columna M.
* Renombre el rango A10:B14 como **tabla**.
* Guarde el libro en formato .xlsx y asígnele su código como nombre del archivo. Por ejemplo: **201725842.xlsx**
* Suba el archivo a Bloque Neón.

